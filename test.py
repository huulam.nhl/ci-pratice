import requests
import json
from datetime import datetime

url = 'http://10.5.11.90:3100/api/prom/push'

headers = {
    'Content-Type': 'application/json'
}

data = {
    'streams': [
        {
            'labels': '{job="my-job"}',
            'entries': [
                {
                    'ts': now,
                    'line': 'This is a log message'
                }
            ]
        }
    ]
}

response = requests.post(url, headers=headers, data=json.dumps(data))

if response.status_code == 204:
    print('Log message sent successfully')
else:
    print('Failed to send log message:', response.text)